package com.nagarro.webController;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nagarro.dao.UserDao;
import com.nagarro.model.User;

@WebServlet("/Registration")
public class RegistrationController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("registration.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			register(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void register(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String fullName = request.getParameter("fullname");
		String userName = request.getParameter("username");
		String password = request.getParameter("password");

		User user = new User(fullName, userName, password);
		UserDao userDao = new UserDao();
		List<User> users = userDao.getUserList();
		boolean bool = true;
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getUsername().equals(userName)) {
				bool = false;
			}
		}
		if (bool)
			userDao.saveUser(user);
		response.sendRedirect("index.jsp");
		
	}
}
